import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers


model = keras.Sequential(
    [
        layers.Dense(2, activation='relu', name='Layer 1')
        #, layers.Dense(3, activation='relu', name='Layer 2')
        , layers.Dense(4, name='Output')
    ]
)

x = tf.ones((2,2))
y = model(x)

model.weights
model.summary()

""" Just to know
is equivalent to this function:

# Create 3 layers
layer1 = layers.Dense(2, activation="relu", name="layer1")
layer2 = layers.Dense(3, activation="relu", name="layer2")
layer3 = layers.Dense(4, name="layer3")

# Call layers on a test input
x = tf.ones((3, 3))
y = layer3(layer2(layer1(x)))

"""

model2 = keras.Sequential()
model2.add(layers.Dense(10, activation='relu', name='a'))
model2.add(layers.Dense(3, activation='relu', name='b'))
model2.add(layers.Dense(4))

x = tf.ones((1,10))
y = model2(x)
model2.summary()
model2.save()
model2.predict( [ [1,2,3,4,5,6,7,8,9,10] ] )