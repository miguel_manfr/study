import numpy
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from numpy import genfromtxt
import numpy as np
import pandas as pd
from tensorflow.keras import utils


data = genfromtxt('iris.txt', delimiter=',', dtype=None, encoding=None)
x = [ list(item[0:4]) for item in data.astype(list) ]

classes = [  item[4] for item in data.astype(list) ]
y = list()#pd.DataFrame()
for classification in classes:
    dic = dict.fromkeys( set(classes), 0 )
    dic[ classification ] = 1
    #y = y.append(dic, ignore_index=True)
    y.append( list( dic.values()) )

dummy_y = utils.to_categorical(y)

model = keras.Sequential(
    [
        layers.Dense(8, input_dim=4, activation=tf.nn.relu)
        , layers.Dense(3, activation=tf.nn.softmax)
    ]
)

model.summary()

model.compile(optimizer=tf.optimizers.Adam(), loss= tf.losses.categorical_crossentropy, metrics=tf.metrics.categorical_accuracy ) # metrics=['accuracy'])
model.fit(x=x, y=y, epochs=30)

model.predict([x[2]])
y[2]