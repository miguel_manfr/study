import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from numpy import loadtxt

data = loadtxt("pima-indians-diabetes.data.txt", delimiter=',')
total = len(data)
x = data[:int(total*.9),0:8]
y = data[:int(total*.9),8]

val_x = data[int(total*.9):,0:8]
val_y = data[int(total*.9):,8]

model = keras.Sequential(
    [
        #layers.Dense(8, activation='relu'), Funciona igual com ou sem input_dim, mas primeiro layer tem que ter mesmo numero de neuronios de entrada
        layers.Dense(12, input_dim=8, activation='relu'), # Neste caso o Input_dim cria o Layer de entrada e o Hidden com 12 neuronios
        layers.Dense(8, activation='relu'),
        layers.Dense(1, activation=tf.nn.sigmoid)
    ]
)

model.compile( optimizer=tf.optimizers.RMSprop()
               , loss=tf.losses.binary_crossentropy
               , metrics=tf.metrics.binary_accuracy)

model.fit(x= x, y=y, epochs=500, validation_data=(val_x, val_y), validation_steps=5, steps_per_epoch=5 )

model.predict( [ [ 6,148,72,35,0,33.6,0.627,50 ] ])
model.summary()
