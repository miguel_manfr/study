
import tensorflow as tf
import numpy as np
import logging

from requests.packages.urllib3.packages.six.moves import xrange

logger = tf.get_logger()
logger.setLevel(logging.ERROR)


#Trainig Data
#F to C formula -> c * 1.8 + 32

celsius_q    = np.array([-40, -10,  0,  8, 15, 22,  38],  dtype=float)
fahrenheit_a = np.array([-40,  14, 32, 46, 59, 72, 100],  dtype=float)

celsius_q_validation    = np.array([100, 1, 25, 32],  dtype=float)
fahrenheit_a_validation = np.array([212,  33.8, 77, 89.6],  dtype=float)


for i,c in enumerate(celsius_q):
  print("{} degrees Celsius = {} degrees Fahrenheit".format(c, fahrenheit_a[i]))


#Layers
from tensorflow.keras.layers import Dense

model = tf.keras.Sequential([
                              tf.keras.layers.Dense( 1, input_shape=[1] )
                            ])

model.compile(loss='mean_squared_error'
              , optimizer=tf.keras.optimizers.Adam(0.1)
              #, optimizer=tf.keras.optimizers.RMSprop(learning_rate= 0.02)
              , metrics=['accuracy']
              )
model.summary()

history = model.fit(x = celsius_q
          , y = fahrenheit_a
          , epochs = 300
          , verbose=True
          #, validation_data= ( celsius_q_validation, fahrenheit_a_validation )
        )

#plot
import matplotlib.pyplot as plt
import math
plt.xlabel('Epoch number')
plt.ylabel('Loss Mag.')
plt.plot(history.history['loss'])
plt.show()

math.ceil ( model.predict([45]) )

scores = model.evaluate(celsius_q_validation, fahrenheit_a_validation, verbose=1)